# Zaphod Resources

A collection of some resources for the Zaphod keyboard.

# Zaphod Lite PCB

The [Zaphod Lite](./lite) is a hand solderable keeb using the XIAO family of controllers.

# Cases

## 3dp Case

Latest [STEP and STL files](https://gitlab.com/lpgalaxy/zaphod/-/jobs/artifacts/main/download?job=exports:3dp-case)

## Acrylic Case

Latest [DXF files](https://gitlab.com/lpgalaxy/zaphod/-/jobs/artifacts/main/download?job=exports:acrylic-case)

# Other

## Display Cover

Latest [STEP and STL files](https://gitlab.com/lpgalaxy/zaphod/-/jobs/artifacts/main/download?job=exports:display-cover)

## Battery Cover

Latest [STEP and STL files](https://gitlab.com/lpgalaxy/zaphod/-/jobs/artifacts/main/download?job=exports:battery-cover)

# License

Unless there is a different `LICENSE` file in a subdirectory of this repository, all works are licensed under the Creative Commons [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.
